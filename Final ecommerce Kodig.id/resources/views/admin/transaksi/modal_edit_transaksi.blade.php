<form method="post" action="{{ route('simpan_ubah_transaksi', $data_transaksi->id) }}">
    {{ csrf_field() }}
   
    <label> Status </label>
        <select type="text" name="status" value="{{ $data_transaksi->status }}" class="form-control">
            @if($data_transaksi->status==1)
            <option value="1" >Verifikasi</option>
            @else
            <option value="1" >Verifikasi</option>
            <option value="0" >Belum Verif</option>
            @endif
        </select>
    </div>  
    <div class="form-group">
        <label> Keterangan</label>
        <input type="text" name="keterangan" value="{{ $data_transaksi->keterangan }}" class="form-control" placeholder="Jenis Produk ..">
    </div>
    <div class="form-group">
        <label> tanggal Verif</label>
        <input type="date" name="tgl_verif"value="{{ $data_transaksi->tgl_verif }}" class="form-control" placeholder="..">
    </div>
    <div><center> 
        <a href="{{url('/data_file/transaksi/'.$data_transaksi->bukti_tf) }}">Bukti Transfer</a></div>
        <div>
        <img width="300px" class="center" src="{{url('/data_file/transaksi/'.$data_transaksi->bukti_tf) }}">
    </div></center></div>
    <button type="submit" class="btn btn-success">Simpan</button>
    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button> 
</form>
