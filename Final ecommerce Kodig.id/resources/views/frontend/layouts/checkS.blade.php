
@extends('frontend.layouts.master')
@section('content')
<div class="c-layout-page">
        <!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
        <div class="c-layout-breadcrumbs-1 c-subtitle c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
            <div class="container">
                <div class="c-page-title c-pull-left">
                    <h3 class="c-font-uppercase c-font-sbold">Checkout Complete</h3>
                    <h4 class="">Page Sub Title Goes Here</h4>
                </div>
                <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                    <li>
                        <a href="shop-checkout-complete.html">Checkout Complete</a>
                    </li>
                    <li>/</li>
                    <li class="c-state_active">Jango Components</li>
                </ul>
            </div>
        </div>
        <!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-2 -->
        <!-- BEGIN: PAGE CONTENT -->
        <div class="c-content-box c-size-lg c-overflow-hide c-bg-white">
            <div class="container">
                
                <div class="c-shop-order-complete-1 c-content-bar-1 c-align-left c-bordered c-theme-border c-shadow">
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">Checkout Completed</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div>
                    <div class="c-theme-bg">
                        <p class="c-message c-center c-font-white c-font-20 c-font-sbold">
                            <i class="fa fa-check"></i> Thank you. Your order has been received. 
                            <i class="c-message c-center c-font-white c-font-20 c-font-sbold"> Transfer : herlambang 12312312321312313</i>
                        </p>
                            
                    </div>
                    
                    <!-- BEGIN: ORDER SUMMARY -->
                    <div class="row c-order-summary c-center">
                            @foreach($data as $p)
                        <ul class="c-list-inline list-inline">
                            <li>
                                <h3>Order Number</h3>
                                
                                
                                <h1># {{$p->nomor_transaksi}}</h1>
                                
                            </li>
                            <li>
                                <h3>Date Purchased</h3>
                                <p>{{$p->tgl_transaksi}}</p>
                            </li>
                            <li>
                                <h3>Total Payable</h3>
                                <p>$ {{$p->total_harga}}</p>
                            </li>
                            <li>
                                <h3>Address</h3>
                                <p> {{$p->alamat_kirim}} </p>
                            </li>
                        </ul>
                        @endforeach
                    </div>

                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">Detail Transaksi</h3>
                        <div class="c-line-center c-theme-bg"></div>
                    </div>

                    <div class="c-shop-cart-page-1">
                        <div class="row c-cart-table-title">
                            <div class="col-md-2 c-cart-image">
                               </div>
                            <div class="col-md-4 c-cart-desc">
                                <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Detail</h3>
                            </div>
                            
                            <div class="col-md-1 c-cart-qty">
                                <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Qty</h3>
                            </div>
                            <div class="col-md-2 c-cart-price">
                                <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Unit Price</h3>
                            </div>
                            <div class="col-md-1 c-cart-total">
                                <h3 class="c-font-uppercase c-font-bold c-font-16 c-font-grey-2">Total</h3>
                            </div>
                            <div class="col-md-1 c-cart-remove"></div>
                        </div>
                        <!-- BEGIN: SHOPPING CART ITEM ROW -->
                        
                        <div class="row c-cart-table-row">
    
                            <?php $total = 0?>
    
                            @if(session('cart'))
                            @foreach(session('cart') as $id => $details)
                            
                            <?php $total += $details['price'] * $details['quantity'] ?>
                         
                            
                            <div class="col-md-2 col-sm-3 col-xs-5 c-cart-image">
                                </div>
                            <div class="col-md-4 col-sm-9 col-xs-7 c-cart-desc">
                                <h3>
                                    <a href="shop-product-details-2.html" class="c-font-bold c-theme-link c-font-22 c-font-dark">{{ $details['name'] }}</a>
                                </h3>
                                
                            </div>
      
                            <div class="col-md-1 col-sm-3 col-xs-6 c-cart-qty">
                                <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">QTY</p>
                                <div >
                                    
                                    <p class="c-cart-price c-font-bold">{{ $details['quantity'] }}</p>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-6 c-cart-price">
                                <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Unit Price</p>
                                <p class="c-cart-price c-font-bold">$ {{ $details['price'] }}</p>
                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-6 c-cart-total">
                                <p class="c-cart-sub-title c-theme-font c-font-uppercase c-font-bold">Total</p>
                                <p class="c-cart-price c-font-bold">$ {{ $details['price'] * $details['quantity'] }}</p>
                            </div>
                            
                              
                        @endforeach
                        @endif
                        </div>
                    
                        <!-- END: SHOPPING CART ITEM ROW -->
                        <!-- BEGIN: SHOPPING CART ITEM ROW -->
       
                        <!-- END: SHOPPING CART ITEM ROW -->
                        <!-- BEGIN: SUBTOTAL ITEM ROW -->
                        <div class="row">
                            <div class="c-cart-subtotal-row c-margin-t-20">
                                <div class="col-md-1 col-md-offset-9 col-sm-6 col-xs-6 c-cart-subtotal-border">
                                    <h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Subtotal</h3>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-6 c-cart-subtotal-border">
                                    <h3 class="c-font-bold c-font-16">$ {{ $total }}</h3>
                                </div>
                            </div>
                        </div>
                        <!-- END: SUBTOTAL ITEM ROW -->
                        <!-- BEGIN: SUBTOTAL ITEM ROW -->
                  
                        <!-- END: SUBTOTAL ITEM ROW -->
                        <!-- BEGIN: SUBTOTAL ITEM ROW -->
                        <div class="row">
                            <div class="c-cart-subtotal-row">
                                <div class="col-md-1 col-md-offset-9 col-sm-6 col-xs-6 c-cart-subtotal-border">
                                    <h3 class="c-font-uppercase c-font-bold c-right c-font-16 c-font-grey-2">Total</h3>
                                </div>
                                <div class="col-md-2 col-sm-6 col-xs-6 c-cart-subtotal-border">
                                    <h3 class="c-font-bold c-font-16">$ {{ $total }}</h3>
                                </div>
                            </div>
                        </div>
                        <!-- END: SUBTOTAL ITEM ROW -->
                        
                    </div>
                    
                    
                    
                
                    <!-- END: ORDER SUMMARY -->
                    <!-- BEGIN: BANK DETAILS -->

                    <!-- END: CUSTOMER DETAILS -->
                </div>
            </div>
        </div>
        <!-- END: PAGE CONTENT -->
    </div>

    @endsection
    