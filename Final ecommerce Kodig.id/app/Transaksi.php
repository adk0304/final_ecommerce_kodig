<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    // 
    protected $table = "transaksi";
    protected $primaryKey = "id";
	protected $fillable = ['id','nomor_transaksi','tgl_transaksi','total_harga','alamat_kirim','invoice','status','keterangan','tgl_verif','bukti_tf','user_id'];
    public function dtransaksi(){
        return $this->hasMany('App\Dtransaksi');
}

    public function user(){
    	return $this->belongsTo('App\User');
	}
}
